@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="{{ $user->profile->profileImage()}}" class="rounded-circle w-100">        </div>
        <div class="col-9 pt-5">
            <div class="d-flex align-items-center pb-3">
                <div class="h4">{{ $user->username }}</div>
                {{-- <follow-button class="btn btn-primary ml-3 mb-1">Follow</follow-button> --}}
                <follow-button user-id="{{ $user->id }}"></follow-button>
                {{-- <follow-button user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-button> --}}
            </div>
            <div class="d-flex">
                <div class="pr-5"><strong>111</strong> posts</div>
                <div class="pr-5"><strong>111</strong> followers</div>
                <div class="pr-5"><strong>111</strong> following</div>
            </div>
            <div class="pt-2"><strong>{{$user->profile->title}}</strong></div>
            <div>{{$user->profile->description}}</div>
            <div><a href="#">{{$user->profile->url}} </a></div>
        </div>
    </div>

    <div class="row pt-4">
        @can('update', $user->profile)
            <a href="/profile/{{ $user->id }}/edit" type="button" class="btn btn-outline-dark mx-3 btn-block">Edit Your Profile!</a>
            <a href="/p/create" type="button" class="btn btn-outline-dark mx-3 btn-block">Add Post!</a>
        @endcan
    </div>

    <div class="row pt-5">
        @foreach($user->posts as $post)
            <div class="col-4 pb-4">
                <a href="/p/{{ $post->id }}">
                    <img src="/storage/{{ $post->image }}" class="w-100">
                </a>
            </div>
        @endforeach
    </div>

</div>
@endsection
